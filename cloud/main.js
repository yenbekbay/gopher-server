/*
DEFINITIONS & JOBS
*/

Parse.Cloud.define("getClasses", function(request, response) {
  var termId = request.params.termId; // Parses term (Semester)
  var campusAbbr = request.params.campusAbbr; // Parses campus location
  var catalogNumber = request.params.catalogNumber; // Parses class number (e.g. 1149)
  if (catalogNumber.length != 4 || catalogNumber.length != 5) { // Throws error dialogue if class number is invalid length
    response.error("The class ID number must be 4 or 5 characters. (ex: 1149)");
  }
  var subjectId = request.params.subjectId; // Parses department ID (e.g. CSCI)
  if (subjectId.length != 4) { // Logs error if subjectId is invalid length
    response.error("The department ID must be exactly 4 characters. (ex: CSCI)");
  }
  var url = "http://courses.umn.edu/campuses/" + campusAbbr + "/terms/" + termId + "/classes.json";
  var params = "subject_id=" + subjectId + "," + "catalog_number=" + catalogNumber;
  Parse.Cloud.httpRequest({
    url: url,
    params: {
      q : params
    },
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  }).then(function(httpResponse) { // Runs if URL is valid
    response.success(httpResponse.data.courses);
  },function(httpResponse) { // Returns error if URL is invalid
    response.error("Something went wrong with the request");
  });
});

Parse.Cloud.define("getCampuses", function(request, response) {
  var Campus = Parse.Object.extend("Campus");
  var campusQuery = new Parse.Query(Campus);
  campusQuery.find({
    success: function(campuses) {
      if (campuses.length > 0) {
        response.success(campuses);
      } else {
        loadCampuses({
          success: function(campuses) {
            response.success(campuses);
          },
          error: function(error) {
            response.error(error);
          }
        });
      }
    },
    error: function(error) {
      loadCampuses({
        success: function(campuses) {
          response.success(campuses);
        },
        error: function(error) {
          response.error(error);
        }
      });
    }
  });
});

Parse.Cloud.define("getTerms", function(request, response) {
  var Term = Parse.Object.extend("Term");
  var termQuery = new Parse.Query(Term);
  termQuery.find({
    success: function(terms) {
      if (terms.length > 0) {
        response.success(terms);
      } else {
        loadTerms({
          success: function(terms) {
            response.success(terms);
          },
          error: function(error) {
            response.error(error);
          }
        });
      }
    },
    error: function(error) {
      loadTerms({
        success: function(terms) {
          response.success(terms);
        },
        error: function(error) {
          response.error(error);
        }
      });
    }
  });
});

/*
DEFINITIONS & JOBS
*/

function loadCampuses(response) {
  var url = "http://courses.umn.edu/campuses.json";
  Parse.Cloud.httpRequest({
    url: url,
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  }).then(function(httpResponse) { // Runs if URL is valid
    var campuses = httpResponse.data.campuses;
    var Campus = Parse.Object.extend("Campus");
    var campusObjects = [];
    var campusIndex = 0;
    saveNextCampus();
    function saveNextCampus() {
      if (campusIndex >= campuses.length) {
        response.success(campusObjects);
      } else {
        abbreviation = campuses[campusIndex].abbreviation;
        var campus = new Campus();
        campus.save({
          abbreviation: abbreviation,
          description: campusDescriptions.abbreviation
        }, {
          success: function(campus) {
            campusObjects.push(campus);
            campusIndex++;
            saveNextCampus();
          },
          error: function(campus, error) {
            response.error(error);
          }
        });
      }
    }
  },function(httpResponse) { // Returns error if URL is invalid
    response.error("Something went wrong with the request");
  });
}

function loadTerms(response) {
  var url = "https://onestop.umn.edu/lib/course_fees/get_academic_terms.php";
  Parse.Cloud.httpRequest({
    url: url,
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  }).then(function(httpResponse) { // Runs if URL is valid
    var terms = httpResponse.data;
    var Term = Parse.Object.extend("Term");
    var termObjects = [];
    var termIndex = 0;
    saveNextTerm();
    function saveNextTerm() {
      if (termIndex >= terms.length) {
        response.success(termObjects);
      } else {
        var term = new Term();
        term.save(terms[termIndex], {
          success: function(term) {
            termObjects.push(term);
            termIndex++;
            saveNextTerm();
          },
          error: function(term, error) {
            response.error(error);
          }
        });
      }
    }
  },function(httpResponse) { // Returns error if URL is invalid
    response.error("Something went wrong with the request");
  });
}

var campusDescriptions = {
  "UMNMO": "University of Minnesota, Morris",
  "UMNCR": "University of Minnesota, Crookston",
  "UMNRO": "University of Minnesota, Rochester",
  "UMNTC": "University of Minnesota, Twin Cities",
  "UMNDL": "University of Minnesota, Duluth"
};
